// try {
//   require('electron-reloader')(module)
// } catch (_) {}

const { app, BrowserWindow, ipcMain } = require('electron')
const fs = require('fs');

let mainWindow

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800, 
    height: 400,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    }
  });
  mainWindow.loadFile('index.html')

  mainWindow.webContents.openDevTools()

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('files', (e, a) => {
  console.log(a);
  fs.stat(a, (err, stats) => {
    if (err) {
        console.log(`File doesn't exist.`);
        return;
    }
    console.log(stats.size);
  });
});