const { ipcRenderer } = require('electron')

const targetImageInput = document.querySelector('#targetImage');

targetImageInput.addEventListener('change', (e) => {
    console.log(e.target.files);
    ipcRenderer.send('files', e.target.files[0].path);
});
